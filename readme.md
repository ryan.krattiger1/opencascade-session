## OpenCascade session for smtk

The OpenCascade session adds a CAD-style model representation
to SMTK-based applications, like modelbuilder and aeva. This
will initially be used in the AEVA project.

+ For more information, see [aeva's website at simtk.org](https://simtk.org/projects/aeva-apps)
+ For application binaries that use this session,
  see [aeva downloads at data.kitware.com](https://data.kitware.com/#collection/5e387f9caf2e2eed3562242e)
    + `custom` contains custom builds
    + `continuous` contains builds queued by our continuous integration system.


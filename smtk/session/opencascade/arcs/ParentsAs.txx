//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ParentsAs_txx
#define smtk_session_opencascade_ParentsAs_txx

#include "smtk/session/opencascade/arcs/ParentsAs.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"

namespace smtk
{
namespace session
{
namespace opencascade
{
template <typename XToType>
bool ParentsAs<XToType>::visit(
  const std::function<bool(const typename ParentsAs<XToType>::ToType&)>& fn) const
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_from.resource());
  auto session = resource->session();

  if (auto shape = m_from.data())
  {
    // If our shape is a compound, it has no parent
    auto shapeType = shape->ShapeType();
    if (shapeType == TopAbs_COMPOUND)
    {
      return true;
    }

    TopTools_IndexedDataMapOfShapeListOfShape map;
    TopExp::MapShapesAndAncestors(resource->compound(), shapeType,
      static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1), map);

    const TopTools_ListOfShape& parents = map.FindFromKey(*shape);

    for (const TopoDS_Shape& parent : parents)
    {
      auto uid = session->findID(parent);
      if (!uid)
      {
        continue;
      }
      auto node = dynamic_cast<ToType*>(resource->find(uid).get());
      if (!node)
      {
        continue;
      }
      if (fn(*node))
      {
        return true;
      }
    }
  }
  return false;
}

template <typename XToType>
bool ParentsAs<XToType>::visit(const std::function<bool(typename ParentsAs<XToType>::ToType&)>& fn)
{
  auto resource = static_pointer_cast<smtk::session::opencascade::Resource>(m_from.resource());
  auto session = resource->session();

  if (auto shape = m_from.data())
  {
    // If our shape is a compound, it has no parent
    auto shapeType = shape->ShapeType();
    if (shapeType == TopAbs_COMPOUND)
    {
      return true;
    }

    TopTools_IndexedDataMapOfShapeListOfShape map;
    TopExp::MapShapesAndAncestors(
      *shape, shapeType, static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1), map);

    Standard_Integer i, nE = map.Extent();
    for (i = 1; i < nE; i++)
    {
      auto uid = session->findID(map.FindKey(i));
      if (!uid)
      {
        continue;
      }
      auto node = dynamic_cast<ToType*>(resource->find(uid).get());
      if (!node)
      {
        continue;
      }
      if (fn(*node))
      {
        return true;
      }
    }
  }
  return false;
}
}
}
}

#endif

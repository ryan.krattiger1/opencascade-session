# Classes have both a header and a source file:
set(classes
  IconConstructor
  Operation
  Session
  Registrar
  Resource
  Compound
  CompSolid
  Shape
  Solid
  Shell
  Face
  Wire
  Edge
  Vertex
)
# Operations have header, source, and sbt files:
set(operators
  CreateBox
  CreateResource
  Cut
  Import
)
# This should be initialized with a list of standalone header files:
set(headers
  queries/SelectionFootprint.h
  arcs/ChildrenAs.h
  arcs/ChildrenAs.txx
  arcs/ParentsAs.h
  arcs/ParentsAs.txx
)
# This should be initialized with a list of standalone source files:
set(sources
)

# Expand class names into actual files
foreach(class ${classes})
  list(APPEND sources ${class}.cxx)
  list(APPEND headers ${class}.h)
endforeach()

# Expand operations into actual files plus a target for sbt encoding.
foreach (operator ${operators})
  smtk_encode_file("${CMAKE_CURRENT_SOURCE_DIR}/operators/${operator}.sbt"
  TARGET_OUTPUT targetName)
  list(APPEND sources operators/${operator}.cxx)
  list(APPEND headers operators/${operator}.h)
  list(APPEND _dependencies ${targetName})
endforeach()

file(GLOB SESSION_FILES "*.h" "*.cxx")
source_group("session" FILES ${SESSION_FILES})
file(GLOB OPERATIONS_FILES "operators/*.h" "operators/*.cxx" "operators/*.sbt")
source_group("session\\operators" FILES ${OPERATIONS_FILES})
file(GLOB VTK_FILES "vtk/*.h" "vtk/*.cxx")
source_group("session\\vtk" FILES ${VTK_FILES})

# Build the library:
add_library(smtkOpencascadeSession ${sources})
add_dependencies(smtkOpencascadeSession ${_dependencies})
target_include_directories(smtkOpencascadeSession
  PUBLIC
    $<BUILD_INTERFACE:${OpenCASCADE_INCLUDE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${OpenCASCADE_INCLUDE_DIR}>
)

target_link_libraries(smtkOpencascadeSession
  PUBLIC
    smtkCore
    TKGeomBase
    TKBRep
    TKernel
    TKLCAF
    TKXCAF
    TKSTEP
    TKXDESTEP
    TKIGES
    TKXDEIGES
)
# smtk_export_header(smtkOpencascadeSession Exports.h)
# smtk_install_library(smtkOpencascadeSession)
# smtk_public_headers(smtkOpencascadeSession ${headers})
generate_export_header(smtkOpencascadeSession EXPORT_FILE_NAME Exports.h)

# Install the header files
smtk_get_kit_name(name dir_prefix)
install(
  FILES
    ${headers}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    include/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkOpencascadeSession
  EXPORT  OpencascadeSession
  ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
  LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
  RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
  PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix}"
)


if (SMTK_ENABLE_VTK_SUPPORT)
  set(module_files "${CMAKE_CURRENT_SOURCE_DIR}/vtk/vtk.module")
  vtk_module_scan(
    MODULE_FILES ${module_files}
    PROVIDES_MODULES vtk_modules
    HIDE_MODULES_FROM_CACHE ON
    WANT_BY_DEFAULT ON)
  vtk_module_build(
    MODULES ${vtk_modules}
    PACKAGE SMTKOpencascadeExt
    INSTALL_EXPORT SMTKOpencascadeModules
    CMAKE_DESTINATION ${CMAKE_INSTALL_CMAKEDIR}
    HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
    TEST_DIRECTORY_NAME "NONE")

  if (SMTK_ENABLE_PARAVIEW_SUPPORT)
    paraview_plugin_scan(
      PLUGIN_FILES "${CMAKE_CURRENT_SOURCE_DIR}/plugin/paraview.plugin"
      PROVIDES_PLUGINS paraview_plugins
      ENABLE_BY_DEFAULT ON
      HIDE_PLUGINS_FROM_CACHE ON
    )
    paraview_plugin_build(
      HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
      LIBRARY_SUBDIRECTORY "smtk-${smtk_VERSION}"  # put the plugin .xml with smtk plugins.
      PLUGINS ${paraview_plugins}
      PLUGINS_FILE_NAME "smtk.opencascadesession.xml"
      AUTOLOAD ${paraview_plugins}
      INSTALL_EXPORT OpencascadeSessionPlugins
      CMAKE_DESTINATION ${CMAKE_INSTALL_CMAKEDIR}
      ADD_INSTALL_RPATHS ON
      TARGET opencascade_session_paraview_plugins
    )
  endif()
endif()

# if (SMTK_ENABLE_PYTHON_WRAPPING)
#   add_subdirectory(pybind11)
# endif()

if (OPENCASCADE_ENABLE_TESTING)
  add_subdirectory(testing)
endif()

cmake_minimum_required(VERSION 3.10)

# Project name and version
project(opencascade-session VERSION 0.1)

# Add our cmake directory to the module search path
list(INSERT CMAKE_MODULE_PATH 0
  ${PROJECT_SOURCE_DIR}/cmake)

# This plugin requires C++11
if(NOT DEFINED CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 11)
  set(CMAKE_CXX_STANDARD_REQUIRED True)
  set(CMAKE_CXX_EXTENSIONS False)
endif()

# Use shared libs, otherwise we don't get a loadable plugin
set(BUILD_SHARED_LIBS True)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

include(ocOptions)
include(ocDependencies)
include(ocSettings)

add_subdirectory(smtk)

include(ocPackaging)
